$(document).ready(function () {
  const url = "https://bj.scjn.gob.mx/api/versiones-taquigraficas/"; // "https://bj.scjn.gob.mx/api/versiones-taquigraficas/"; //"http://localhost:8082/";
  const urlVtaquigraficas = url + "versionesTaq/asuntos?";
  const urlAnios = url + "versiones-taquigraficas-m/fechas-sesiones";
  const urlAsuntos = url + "versionesTaq/tiposAsuntos";
  var urlCountFiltrosTemas = url + "versionesTaq/countAsuntos?";
  const dataMeses = [
    { key: "01", value: "Enero" },
    { key: "02", value: "Febrero" },
    { key: "03", value: "Marzo" },
    { key: "04", value: "Abril" },
    { key: "05", value: "Mayo" },
    { key: "06", value: "Junio" },
    { key: "07", value: "Julio" },
    { key: "08", value: "Agosto" },
    { key: "09", value: "Septiembre" },
    { key: "10", value: "Octubre" },
    { key: "11", value: "Noviembre" },
    { key: "12", value: "Diciembre" },
  ];

  var selectMes = null;
  var selectAnio = null;
  var isFiltros = [];
  var isFiltrosTemas = false;
  var tipoTemaFil = "";
  var tipoRowData = 0;
  var pagina = 1;
  var columna_extra = "";
  var titulo = " Pleno - Versiones Taquigráficas";  
  var sizeColumVideo = "10%";
  var sizeColumVAsuntos = "70%";
  $("#idReiniciar").hide();
  $("#idVTaquigraficas").click(function () {
    $("#idSeccionVTaquigraficas").load("./vTaquigraficas.html");
    $("#idSeccionVTaquigraficas").show();
    getVersionesTaquigraficas(1, 10);
    getTiposAsuntos();
    getAniosSesionesVersionesTaquigraficas();
  });

  async function getVersionesTaquigraficas(page, size) {
    showLoading(true);
    deteccionUrlTipoPlantilla();
    const result = await fetch(getUrl(page, size));
    const data = await result.json();
    getAsignarDatosGenerales(data);
    $.each(data.content, function (index, element) {
      getAsignaData(element);
    });
    showLoading(false);
    showPaginacion(data);
  }

  function showPaginacion(data) {
    if (data.totalElements > 0 && data.totalPages > 0) {
      $("#IdNavPagination").show();
      paginacion(data.totalPages, data.numberOfElements);
      $("#IdDivSinResultados").hide();
    } else {
      $("#IdNavPagination").hide();
      $("#IdDivSinResultados").show();
    }
  }

  //Crea y asigna valores iniciales al elemnto Bootpag (paginacion), cuando se cargan resultados
  function paginacion(totalPages, numberOfElements) {
    $("#show_paginator").bootpag({
      total: totalPages, // total pages
      page: pagina, // default page
      maxVisible: numberOfElements, // visible pagination
      //leaps: true // next/prev leaps through maxVisible
      firstLastUse: true,
      maxVisible: 10,
      /* wrapClass: "pagination container-paginacion", */
    });
  }

  //se activa al cambiar de numero en la paginacion
  $("#show_paginator").on("page", function (event, num) {
    pagina = num;
    getVersionesTaquigraficas(num, 10);
  });

  function getAsignarDatosGenerales(data) {
    limpiaTabla();
    asignaColumnas();
    getCountResultados(data);
  }

  function getCountResultados(data) {
    data.totalElements <= 10
      ? $("#IdCountResultados").text(
          "1 a " +
            data.totalElements +
            "  de " +
            data.totalElements +
            " resultados"
        )
      : getCountResultadosPainacion(data.number, data.totalElements);
  }

  async function getCountResultadosSentenciasByTema() {
    const resultCount = await fetch(urlCountFiltrosTemas);
    const count = await resultCount.json();
    displayCountResultadosSentencias("block");
    $("#IdCountResultadosSentencias").text(
      "Total de sentencias encontradas: " + count
    );
  }

  function getCountResultadosPainacion(pagina, total) {
    if (pagina == 0) {
      $("#IdCountResultados").text("1 a 10 de " + total + " resultados");
    } else {
      let inicial = pagina + "1";
      let final = parseInt(inicial) + 9;
      $("#IdCountResultados").text(
        inicial + " a " + final + " de " + total + " resultados"
      );
    }
  }

  function limpiaTabla() {
    $("#dtVerTaquigraficas > thead").remove();
    $("#dtVerTaquigraficas > tbody > tr").remove();
  }

  function deteccionUrlTipoPlantilla() {
    if (sitio == null || sitio == "versiones-taquigraficas") {
      columna_extra = "Documento";
      titulo = 'Pleno - Versiones Taquigráficas';
      document.getElementById("IdUbicacionVTaq").innerHTML =
        '<a href="">Pleno - Versiones Taquigráficas </a>' ;
      sizeColumVideo = "10%";
      sizeColumVAsuntos = "70%";
    } else if (sitio == "videoteca") {
      columna_extra = "Video";
      titulo = 'Videoteca de Sesiones';
      document.getElementById("IdUbicacionVTaq").innerHTML =
      '<a href="">Videoteca de Sesiones </a>'; 
      sizeColumVideo = "35%";
      sizeColumVAsuntos = "45%";
    }
    setTimeout(() => {
      document.getElementById("IdTitulo").innerHTML = titulo;
    }, 1000);
  }  

  function asignaColumnas() {
    if (isFiltros.length == 0) {
      $("#dtVerTaquigraficas").append(
        ` <thead>
                <tr class"col-12 col-xl-12 col-lg-12 col-md-12 col-sm-12">
                    <th class="" style="width: 60%;">Fecha
                    </th>              
                    <th class="" style="width: 40%;">${columna_extra}
                    </th>
                </tr>
            </thead>`
      );
      tipoRowData = 0;
    } else {
      isFiltros.forEach(function (item, index) {
        //console.log(item, index);
        switch (item) {
          case "Asunto":
          case "Expediente":
            $("#dtVerTaquigraficas").append(
              ` <thead>
                            <tr>
                                <th class="" style="width: 35%;">Fecha
                                </th>    
                                <th class="" style="width: 35%;">Asuntos
                                </th>             
                                <th class="" style="width: 30%;">${columna_extra}
                                </th>
                            </tr>
                        </thead>`
            );
            item == "Expediente" ? (tipoRowData = 1) : (tipoRowData = 5);
            break;
          case "Tema":
            $("#dtVerTaquigraficas").append(
              ` <thead >
                                <tr >
                                <th class="" style="width: 20%;">Fecha
                                </th>                                            
                                <th class="" style="width: ${sizeColumVAsuntos};">Asuntos
                                </th>
                                <th class="" style="width: ${sizeColumVideo};">
                                ` +
                columna_extra +
                `     </th>
                                </tr>
                            </thead>`
            );

            switch (tipoTemaFil) {
              case "temasFondo":
                tipoRowData = 2;
                break;
              case "temasProcesal":
                tipoRowData = 3;
                break;
              case "Ambos":
                tipoRowData = 4;
                break;
            }
            break;
        }
      });
    }
    $("#dtVerTaquigraficas").append(`<tbody> </tbody>`);
  }

  function getAsignaData(version) {
    icono_video = "";
    if (sitio == null || sitio == "versiones-taquigraficas") {
      icono_video = `<div class="div1">
            <a href="${version.content.urlVT}" target="_blank" rel="noopener noreferrer" title="">
            <i class="fa fa-file-pdf-o" aria-hidden="true"></i> 
            </a>
            </div>`;
    } else if (sitio == "videoteca") {
      icono_video =
        `<div v style="position: relative; padding-bottom: 56.25%; padding-top: 0px; height: 0; overflow: auto; -webkit-overflow-scrolling: touch;">
            <iframe title="Versiones Taquigraficas del Pleno" frameborder="0" style="position: absolute; top: 0; left: 0; width: 100%; height: 100%;" scrolling="no" allowfullscreen src="` +
        version.content.video +
        `" frameborder="0" allowfullscreen></iframe>
        </div>`;
    }
    if (isFiltros.length == 0) {
      $("#dtVerTaquigraficas").append(
        `<tr><td> ${getFormatoFecha(version.content.fechaSesion)}</td>
                <td> ${icono_video} </td></tr>`
      );
    } else {
      switch (tipoRowData) {
        case 5: // Asunto
        case 1: // Expediente
          $("#dtVerTaquigraficas").append(
            `<tr><td> ${getFormatoFecha(version.content.fechaSesion)} </td>
                        <td><div class="text-center"> ${getExpedienteByAsunto(
                          version.content.asuntos,
                          tipoRowData
                        )}</div></td>
                    <td>            
                    ${icono_video}</td></tr>`
          );

          break;
        case 2: //Temas de Fondo
          getRowsTemas(version.content, icono_video, "Temas de Fondo");
          break;
        case 3: //Temas Procesales
          getRowsTemas(version.content, icono_video, "Temas Procesales");
          break;
        case 4: //Temas Ambos
          $("#dtVerTaquigraficas").append(
            `<tr><td> ${getFormatoFecha(
              version.content.fechaSesion
            )}</td>                              
                                    <td>
                                    ${getRowsTemasAmbos(version.content)}
                                   </td>
                                    <td> ${icono_video}</td>
                                    </tr>`
          );
          break;
      }
    }
  }

  function getRowsTemasAmbos(content) {
    let asuntosTemas = `<p style="color: #24135F; border: solid 1px #24135F; "><strong style="color: #24135F;">&nbsp;&nbsp;&nbsp;&nbsp;Temas de Fondo</strong> </p>   
            </br>                         
        ${getAgrupacionTemas(content, "Temas de Fondo")}
        </br>
        <p style="color: #24135F; border: solid 1px #24135F; "><strong style="color: #24135F;">&nbsp;&nbsp;&nbsp;&nbsp;Temas Procesales</strong> </p> 
        </br>                              
        ${getAgrupacionTemas(content, "Temas Procesales")}
       `;
    return asuntosTemas;
  }

  function getRowsTemas(content, icono_video, tipoTema) {
    //well well-lg
    //<div class="panel-heading"> </div>
    $("#dtVerTaquigraficas").append(
      `<tr><td> ${getFormatoFecha(
        content.fechaSesion
      )}</td>                              
                    <td> <p style="color: #24135F; border: solid 1px #24135F; "><strong style="color: #24135F;">&nbsp;&nbsp;&nbsp;&nbsp;${tipoTema}</strong> </p>
                    </br>                              
                    ${getAgrupacionTemas(content, tipoTema)}
                   </td>
                    <td> ${icono_video}</td>
                    </tr>`
    );
  }

  function getAgrupacionTemas(resultados, tipoTema) {
    let contenidoTema = "";
    let highliTemas = "";
    resultados.asuntos.forEach(function (asunto, indice) {
      //console.log("En el índice " + indice + " hay este valor: " + valor.numExpediente);
      if (tipoTema == "Temas de Fondo") {
        asunto.temasFondoAbordados.forEach(function (tema, indice) {
          tema.includes("<strong><mark")
            ? (highliTemas += `<li>${tema}</li>`)
            : "";
        });
      } else {
        asunto.temasProcesalesAbordados.forEach(function (tema, indice) {
          tema.includes("<strong><mark")
            ? (highliTemas += `<li>${tema}</li>`)
            : "";
        });
      }
      let card = `<div class="panel panel-default">
            <div class="panel-heading">${asunto.asuntoAbordado}  ${asunto.numExpediente}</div>
            <div class="panel-body">
            <ul>
             ${highliTemas}
             </ul>
            </div>
          </div> </br>`;
      contenidoTema += card;
    });

    let cardSinDatos = `<div class="panel panel-default">
       
        <div class="panel-body">
        <ul>
        Sin coincidencias
         </ul>
        </div>
      </div> </br>`;

    highliTemas == "" ? (contenidoTema = cardSinDatos) : "";
    return contenidoTema;
  }

  function getExpedienteByAsunto(asuntos, tipoFiltro) {
    let asunto = "";
    if (tipoFiltro == 1) {
      asunto = asuntos.find(
        (x) => x.numExpediente == document.getElementById("IdExpediente").value
      );
    } else if (tipoFiltro == 5) {
      asunto = asuntos.find(
        (x) => x.asuntoAbordado == document.getElementById("IdTipoAsunto").value
      );
    }
    return asunto != ""
      ? asunto.asuntoAbordado + " " + asunto.numExpediente
      : "";
  }

  String.prototype.replaceAt = function (index, replacement) {
    return (
      this.substring(0, index) +
      replacement +
      this.substring(index + replacement.length)
    );
  };

  function getFormatoFecha(fecha) {
    let newFecha = fecha.split("/")[0] + " de ";
    newFecha +=
      dataMeses.find((element) => element.key == fecha.split("/")[1]).value +
      " de ";
    newFecha += fecha.split("/")[2];
    return newFecha;
  }

  function getUrl(page, size) {
    let urlVTaq = "";
    let filtros = busquedaFiltros();
    urlVTaq = urlVtaquigraficas + "page=" + page + "&size=" + size;
    if (filtros != "") {
      urlVTaq = urlVTaq + "&filtros=" + filtros;
      let fil = filtros.split(":")[1];
      if (isFiltrosTemas) {
        urlCountFiltrosTemas += "&filtros=" + filtros;
      }
    }
    return urlVTaq;
  }

  $("#idReiniciar").click(function () {
    $("#IdExpediente").val("");
    //$("#IdAsunto").val("");
    $("#IdSearchTemaByAsunto").val("");
    $("#MesSesion").prop("selectedIndex", 0);
    $("#AnioSesion").prop("selectedIndex", 0);
    $("#IdTipoAsunto").prop("selectedIndex", 0);
    selectMes = null;
    selectAnio = null;
    isFiltros = [];
    tipoTemaFil = "";
    tipoRowData = 0;
    pagina = 1;
    displayCountResultadosSentencias("none");
    $("#idTemaFondo").prop("checked", true);
    getVersionesTaquigraficas(1, 10);
    $("#idReiniciar").hide();
  });

  $("#idSearchFiltros").click(function () {
    $("#idReiniciar").show();
    getVersionesTaquigraficas(1, 10);
  });

  function displayCountResultadosSentencias(mode) {
    setTimeout(() => {
      document.getElementById(
        "IdSeccionCountResultadosSentencias"
      ).style.display = mode;
    }, 300);
  }

  function busquedaFiltros() {
    isFiltros = [];
    let filtros = "";
    let fecha = validacionesFiltrosFechas();
    let inputExpediente = document.getElementById("IdExpediente");
    if (
      inputExpediente != undefined &&
      inputExpediente != null &&
      inputExpediente.value !== ""
    ) {
      filtros += "numExpediente:" + inputExpediente.value + ",";
      isFiltros.push("Expediente");
    }

    let asunto = document.getElementById("IdTipoAsunto");
    if (
      asunto != undefined &&
      asunto != null &&
      asunto.value !== "" &&
      asunto.value !== "Cualquiera"
    ) {
      //console.log(asunto.value);
      filtros += "tipoAsunto:" + asunto.value + ",";
      isFiltros.push("Asunto");
    }

    let temaByAsunto = document.getElementById("IdSearchTemaByAsunto");
    if (
      temaByAsunto != undefined &&
      temaByAsunto != null &&
      temaByAsunto.value !== ""
    ) {
      let tipo = $("input[name=radioTema]:checked", "#formTemas").val();
      filtros += tipo + ":" + temaByAsunto.value + ",";
      tipoTemaFil = tipo;
      isFiltros.push("Tema");
      isFiltrosTemas = true;
    }
    if (fecha != null) {
      filtros += "fechaSesion:" + fecha;
    }
    return filtros;
  }

  function validacionesFiltrosFechas() {
    let filtroFecha = null;
    if (
      selectMes != null &&
      selectMes != "Cualquiera" &&
      selectAnio != null &&
      selectAnio != "Cualquiera"
    ) {
      filtroFecha = "01" + "-" + selectMes + "-" + selectAnio;
    } else if (selectMes != null && selectMes != "Cualquiera") {
      filtroFecha = selectMes;
    } else if (selectAnio != null && selectAnio != "Cualquiera") {
      filtroFecha = selectAnio;
    }
    return filtroFecha;
  }

  $("#MesSesion").on("change", function () {
    if (dataMeses.find((x) => x.value === this.value) != undefined) {
      selectMes = dataMeses.find((x) => x.value === this.value).key;
    } else {
      selectMes = null;
    }
  });

  $("#AnioSesion").on("change", function () {
    selectAnio = this.value;
  });

  function getAniosSesionesVersionesTaquigraficas() {
    var today = new Date();
    var year = today.getFullYear();
    setTimeout(() => {
      var selectAnios = document.getElementById("AnioSesion");
      option = document.createElement('option');
      option.value = option.text = "Cualquiera";
      selectAnios.add( option );

    for (let anio = year; anio >= 2019; anio--) {
      optionA = document.createElement('option');
      optionA.value = optionA.text = anio;
      selectAnios.add(optionA);
    }   
    }, 1000);
  }

  async function getTiposAsuntos() {
    const result = await fetch(urlAsuntos);
    const data = await result.json();
    $.each(data, function (i, asunto) {
      $("#IdTipoAsunto").append(
        $("<option>").text(asunto).attr("value", asunto)
      );
    });
  }

  function showLoading(status) {
    status == true ? $("#idLoading").show() : $("#idLoading").hide();
    status == true ? window.scrollTo(0, 0) : "";
  }
});
